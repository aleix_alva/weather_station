#include<iostream>
#include<fstream>
#include<math.h>
using namespace std;
int main(int argc, char *argv[]) {
	string infile;
	infile = argv[1];
	int n=0;string linea;
	ifstream in1 (infile);
	while(getline(in1,linea)) n++;
	//cout<<"n="<<n<<"\n";
	in1.close();
	int clicks[n];
	float t[n];
	float dt[n];
	ifstream in2 (infile);
	for(int i = 0;i<n;i++) {
		in2>>clicks[i];
		in2>>t[i];
	}	
	dt[0]=t[0];
	for(int i = 1;i<n;i++) {
		dt[i]=t[i]-t[i-1];
	}
	in2.close();
	float dtav=t[n-1]/(float)clicks[n-1];
	float sd=0.;
	for(int i = 0;i<n;i++) {
		sd += pow(dt[i]-dtav,2.);
	}	
	sd=sd/(float)(n-1);
	sd=pow(sd,0.5);
	cout<<dtav<<" "<<sd<<" "<<sd/pow((float)n,0.5)<<"\n";

}//end main
