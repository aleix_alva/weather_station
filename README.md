# Coding a Weather Station with MakeCode for a BBC micro:bit

## intro

Coding a micro:bit v2 to work with a weather:bit micro:bit carrier board (Qwiic) for the SparkFun Weather Meter Kit.

Here we give makecode files, custom-made, without using the official libraries for rain and wind readings, which (at the moment of this writing) have serious mistakes regarding relay bouncing.

## frequency of measurements

This code will print results every 60 s by default. If you want to change this time, edit the initial variable 'espera_long', and take into account that you need to write it in ms. If you want printing every 120 s you need to set espera_long = 120000.

This is the time between reads when it is not raining, because changes are not abrupt and it makes sense to have measurements every minute or longer. However, when it begins raining, it may be interesting to read more frequently. This code, by default, will ready every 5 s when it begins to rain. You can edit this variable, called 'espera_short', also in ms.

When it stops raining, there is a time in which readins are still rapid. This time can be edited as 'rain_patience', but this is an integer. The total time will be rain_patience x espera_short. Once this time passes, readins will again be done every 60 s (or espera_long).

## rain integration

The total amount of collected rain will be integrated into the variable 'rain_integral'. When it stops raining, and once slow readings are resumed (see previous section), this code waits for a whole day to reset the rain_integral to 0. This variable is called 'rain_reset' and you need to set in seconds this time. By default, it has a value of 24 x 3600 s, meaning a day. This way, the rain_integral value will accumulate as it rains, and then it will stay with this value for non-raining 24 h. What if raining resumes before the dry 24 h pass? The rain_integral value will continue growing.

Measuring cumulative rain for longer periods is tricky because the micro:bit can be frozen, hanged or simply reset for whatever reason, and you don't want to trust your cumulative values to such fragile scenario. Also, the micro:bit, by default, lacks an internal clock. It has an internal chronometer, but not a real time clock (RTC). So in order to get statistics from rain it is better to rely on an external data processing. In order to facilitate this, the variable 'rain_cumulative' will give you the total rain accumulated without ever resetting, excepts when it overflows and resets itself back to 0. This way, you can just simply calculate the increments of this value between short times to externally keep track of rain stats. The singularity when the variable overflows has to be taken care of externally as well.

Also notice that the integration is calculated from a click frequency that is averaged over a time interval (a minute by default), so expect some considerable error when the rain rate significantly varies within such interval. 


## rain gauge calibration

One of the most important reasons for me to avoid the weather:bit libraries was the lack of addressing of relay switch effects. Both rain and wind sensors are based on reed relays that close a circuit every time a magnet passes by. The problem is that this process is noisy and, if taken without care, it can count more than one click per event. In the rain gauge case, it counts always more than one event (sometimes two, sometimes three...). So I needed to use other types of event tracking that took into account such noise.

Another issue was that the pre-factors needed to convert the amount of rain clicks to actual mm (or L/m²) were not properly measured in the weather:bit libraries. The collecting area of the rain device is rectangular, measuring 10.9cm · 4.9cm = 53.41 cm². The corners are rounded, so it is even less than that. This means that for 1m²=10000cm² we need to multiply by 10000/53.41=187.230556.

The biggest issue comes from this fact: the clicking rate is not proportional to the rain rate. This can come as a surprise, but it makes sense. As rain becomes more intense, the way water operates the tipping bucket changes. For a slow rain, water accumulates gently on a side of the bucket and no water is lost to the process. However, as rain rate increases, the process of water accumulation becomes dirtier, so that not all water is used with maximum efficiency in the bucket. This is the reason of the nonlinearity. So we need a calibration curve.

We can unmount the gauge to see the tipping bucket:

![bucket](data/bucket.jpg)

The photo is intentionally tilted to remind us of the difficulty and importance of having a well balanced device, something that is not easy on the field. The rain gauge has a bull's eye spirit level that is quite difficult to balance. 

I have measured, with a syringe, the amount of water required to tip the bucket. The results are (in mL):

```
Clockwise : 1.50, 1.80, 1.70, 1.65, 1.68, 1.80, 1.65
CClockwise: 1.70, 1.65, 1.55, 1.55, 1.53, 1.55, 1.65
``` 

The average is 1.64 mL and we can detect some left-right asymmetry. This means that, for a very slow rate, we have around 1.64 mL/click/ga (ga = gauge area in m²).

We can play with units a little:

```
mL/click = (mL/s)·(s/click) => 

(mL/click)·(click/s) = mL/s => 

(L/click)·(click/s) = L/s => 

x ~ a·f

[x] = L/s
[f] = click/s
[a] = L/click
 a  = 0.00164 L/click
```

It is reasonable that, with very light rain, the amount of rain per second is proportional to the click frequency.

In order to account for the non-linear effects, we placed the rain gauge under a tap, for which we assume a constant dripping rate. The gauge was placed on a scale so that the total amount of water weight could be measured. A chronometer (the internal one from the micro:bit) was used to measure the time to reach a certain amount of total water, typically (but not always) 1L. The rain gauge was connected to a micro:bit with a simple program to simply count the clicks. With this setup we control the 'raining' rate and also the clicking rate, independently.

The obtained data was the following:

```
run:          seconds:         clicks:   litres:

1             1124             538       1.0
2             0831             524       1.0
3             0448             486       1.0
4             0308             469       1.0
5             0144             363       1.0
6             1915             308       0.5
7             0426             497       1.0
8             0512             516       1.0
9             0440             276       0.5
10            0918             294       0.5
11            0412             60        0.1
12            0182             111       0.2
13            0523             288       0.3
14            0517             122       0.2

```

Each run was performed with a different (and constant) opening of the tap. With these data, we can calculate the average time between clicks <t>, its standard deviation (sd), accumulated water volume (V) and the number of clicks:

```
<t>      sd        V    clicks

2.08982  0.354856  1.0  538
1.58666  0.102464  1.0  524
0.921821 0.0461389 1.0  486
0.656957 0.0204161 1.0  469
0.397391 0.0245473 1.0  363
6.21826  0.348993  0.5  308
0.857105 0.0406684 1.0  497
0.991971 0.0433087 1.0  516
1.59482  0.0613179 0.5  276
3.12215  0.154967  0.5  294
6.86462  0.695286  0.1  60
1.63914  0.117231  0.2  111
1.81575  0.10582   0.3  288
4.23899  0.171064  0.2  122

```

With these data we can define

```
x = V / <t> / clicks

y = <t> 

```

where x is just (amount of water) / (time), so it is measured in L/s, and where y is measured in seconds per click. I propose to fit a function of the type

```
<t> = b + a/x
```

because for very big x we expect and observe a saturating minimum <t>. Before fitting this function, we can invert it as

```
x = a / (<t> - b)
```

and, since 1/<t> is the frequency f, we can write

```
x = (a/b)·f / ((1/b) - f)
```

Now, for very small frequencies we simplify to

```
x = a·f
```

as we already knew. This means that we don't need to fit for two but for just one parameter, b. Interestingly, if we leave the two parameters as free, we obtain

```
b = 0.223286
a = 0.00142851.
```

but we are really interested in giving statistical weight to the value we directly, measured, so we fix a = 0.00164 and, weighting as 1/<t>² to give more preference to light-rain values, we obtain b = 0.135811. We show the fitting curve:

![calib](data/calib.png)

The type of function has been chosen heuristically, trying to obtain a limiting value of <t> for a high x and an infinite <t> for x = 0.

In numbers, we have

```
x = 0.012075·f / (7.363 - f).
```

This x(f) tells us the litres per second on the gauge as a function of the frequency of clicks. This is what we wanted, since we measure clicks and want to infer a rain rate from it. But a factor remains to be applied, because the collecting area of the gauge is not 1 m² = 10000 cm² but 53.41 cm². Since 10000/53.41=187.230556, we multiply x by this number, to obtain a rain rate (rr) of

```
rr= 2.261·f / (fmax - f)
```
where

```
fmax = 7.363 click/s
[f] = click/s
[rr] = mm/s = l/(m²s).
```

With this, we can directly measure the click frequency and convert it to a fair approximation of the actual rain rate.
In the 'data' folder there is all the data involved in this process, including the standard deviation calculation.

There is more. When it starts raining, if lightly, drops will accumulate on the device without going to the hole. Up to 3 ml of water can be there in the form of drops, stuck there by surface tension.

![drops](data/drops.jpg)

This means that, when it starts raining, and considering that 3ml for the gauge area represent 0.003·187.23 ~ 0.56 mm, don't expect rain to be measured until a thresold of around 0.5 mm is surpassed. Perhaps the thresold is lower, since I didn't place the drops in the photo at random. What is good here is that this initial rain is not lost. Once the gauge is wet enough, all the water there will be measured.

Another interesting issue: when tipping, the bucket will not always empty its whole content. Up to 0.4 ml of water can remain in the concave space. But, as this occurs in both sides, it is expected that this effect does not contribute much to the bucket's mechanism.

![remains](data/remains.jpg)

# wind speed

The measurement of wind speed is somewhat simpler than that of the rain rate. Again, we count a click rate, but in this case there is a clear linearity of such rate with the angular and hence the linear speed of the anemometer cups. 

The cups have a 2 cm radius, and the distance from the centre of the anemometer to the beginning of the cup is 5 cm. This means that the distance from the global centre to the centre of the cup is 7 cm. The perimeter that this cup centre performs is 2·pi·7 = 14·pi ~ 43.98 cm = 0.4398 m. This number, multiplied by the click rate, will give the wind speed in m/s.

# wind direction

The documentation for this collection of reed relays says that the analog value of P1 associate to the wind directions as

```
N  for (886,906)
NE for (692,712)
E  for (395,415)
SE for (478,498)
S  for (564,584)
SW for (799,819)
W  for (968,988)
NW for (939,959)
```

which sorted by such analog value are

```
E  for (395,415)
SE for (478,498)
S  for (564,584)
NE for (692,712)
SW for (799,819)
N  for (886,906)
NW for (939,959)
W  for (968,988).
```

If we slowly move the vane, the values of P1 do not change gradually, but very abruptly. This also means there are values for which we don't decide the direction of wind (and the output is just a question mark). In conclusion, the resolution of wind direction is a poor 45º. I don't know how to improve this for now. Be sure to orient the station with the help of a compass.

The decision I made here is to take the following angle values according to the letters:

```
N  = 0
NE = 45
E  = 90
SE = 135
S  = 180
SW = 225
W  = 270
NW = 315
```

and then to calculate averages of these values within some time interval. Something to be aware of here is the modular arithmetic involved. Imagine we have 45 and 315 we want the average to be 0. We could take wind direction and speed to form a vector, and then to take the average of this vector:

```
vx=v·sin(dir)
vy=v·cos(dir)
```

And once we have the final average vector, make it unitary if you want an average angle. Not trivial, but that is what this code does.

Take into account that the direction of wind is usually written as direction from which the wind comes. To me, this is kind of counterintuitive because I imagine the vector as an arrow and the direction should point the same as its velocity vector. I think this comes from the fact that the shape of the vane also resembles an arrow, and it points to the direction from which wind comes.

Here we show a diagram to clarify the signs of the vector:

![wind](data/wind.png)


# BME280: temperature, humidity and barometric pressure

There are two temperature sensors. One that comes with the micro:bit itself and another coming from the BME280 sensor from the weather:bit carrier. The difference between the two sensors is around 3 ºC, with the former giving the higher values, I suppose due to self heating of the micro:bit. So it is more convenient to use the temperature values from the BME280 sensor.

There are two microbit extensions for this sensor. One is called BME280, like the sensor itself. It seems to read the Pressure correctly. However, temperature and humidity are treated as integer numbers. This made sense some years ago when microbit did not accept floating point numbers, but now it cannot be accepted. I wrote in the corresponding GitHub page, so perhaps the authors will update this.

The other extension is called Weatherbit, in beta stage now. Temperature must be divided by 100 to obtain ºC and Humidity needs to be divided by 1024 to get %. Weirdly, the extension page says to divide by 256 to get the pressure in Pascals, but this is wrong. By comparing with values measured with the other extension I get a factor of around 6.971. For Pressure I am using the BME280 extension.

You can find a PTH_comparison.hex file inside the src folder to experiment with these numbers.

# summary of magnitudes

The code will output data as a single line each time, with space-separated values. The order is the following:

```
Barometric pressure (Pa)
Relative Humidity (%)
Ambient Temperature (ºC)
Dew Temperature (ºC)
Current rain rate (mm/s)
Cumulative episode rain (mm) [rain_integral]
Cumulative rain (mm) [rain_cumulative]
Wind speed (m/s)
Wind vx speed (m/s)
Wind vy speed (m/s)
Wind angle (º)
Wind direction (letter or ?)
```

The values of pressure, humidity, temperature, current rain rate, wind speed and wind angle are shown as averages within a time interval, while the rest are either instantaneous or cumulative. The value for averaging time is set through the variable 'escribir', which is set by default to 'espera_long', which by default is 60000 ms or 60 s. 

The serial output is printed through the serial USB connection with baud rate 115200. If you prefer to store values in the sd card that comes from the kit, go to the initial code and set serial redirect to: TX P14 and RX P14.

## Funding:

The "Verd de Proximitat BCN" project has received the funding support of the Ajuntament de Barcelona and "la Caixa" Foundation in the framework of the Barcelona Science Plan 2020-2023.

## Licence:

Copyright (Aleix Alva, 2024).

Licensed under the EUPL-1.2 or later.

You can find the full licence text in the file licence.txt at the root of this repository.
